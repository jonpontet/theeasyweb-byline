/**
 * The Easy Web Byline (aka credit)
 */

( function( window, document ) {
    var scripts = document.getElementsByTagName( 'script' ),
        div = document.createElement( 'div' ),
        a = document.createElement( 'a' ),
        aHost = document.createElement( 'a' ),
        span1 = document.createElement( 'span' ),
        span2 = document.createElement( 'span' ),
        img = document.createElement( 'img' ),
        baseURI,
        imgSrc,
        el,
        style = 'black',
		align,
        styleBlack = 'color:#222;text-decoration:none',
        styleWhite = 'color:#fff;text-decoration:none',
        logoBlack = '/bl/bl@2x.png',
        logoWhite = '/bl/bl-white@2x.png',
		title = 'The Easy Web Company';
    
    // Get this SCRIPT tag
    for (var i = 0; i < scripts.length; i++) {
        if ( /bl.js/i.test( scripts[i].getAttribute( 'src' ) ) ) {
            el = scripts[i];
        }
    }
    
    if ( typeof el == 'object' && 'nodeType' in el && el.nodeType === 1 ) {
        
        // Get baseURI of script
        aHost.href = el.src;
        baseURI = aHost.protocol + '//' + aHost.host;
        
        // Get style attribute
        if ( el.getAttribute( 'data-style' ) ) {
            style = el.getAttribute( 'data-style' );
        }
        
        // Get align attribute
        if ( el.getAttribute( 'data-align' ) ) {
            align = el.getAttribute( 'data-align' );
        }
        
        // div
        div.style = ( align == 'center' ) ? 'margin:20px 0;text-align:center' : 'display:inline-block;margin:0 20px;text-align:center';
        
        // anchor
        a.href = baseURI;
        a.target = '_blank';
        a.style = ( style == 'black' ) ? styleBlack : styleWhite;
        
        // image
        imgSrc = baseURI;
        imgSrc += ( style == 'black' ) ? logoBlack : logoWhite;
        img.src = imgSrc;
        img.setAttribute( 'alt', title );
        img.setAttribute( 'title', title );
        img.style = 'position:relative;top:3px;vertical-align:middle;height:23px;';
        
        // text in anchor
        span1.textContent = 'An ';
        span2.textContent = ' creation.';
        a.appendChild( span1 );
        a.appendChild( img );
        a.appendChild( span2 );
        div.appendChild( a );
        
        // insert before this SCRIPT tag
        el.insertAdjacentHTML( 'beforebegin', div.outerHTML);
        
    }
} )( window, document );